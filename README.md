# Ink Addons

This repository contains a collection of useful addons, i.e. themes, icon sets, symbol sets, filters, templates etc., for Inkscape that are publicly available as git repositories

To clone this repository, including its submodules, also updating each submodule to its latest version, do: 

`git clone --recurse-submodules https://gitlab.com/Moini/ink_addons.git`

Each included third-party repository has its own directory structure. Be sure to read the individual addon's documentation.

Please note each addon's license before you use, edit or redistribute it.

If you have an addon that you would like to see added here, would like to expand an addon's description in this README file, or would like to otherwise see this repository updated, please [open an issue](https://gitlab.com/Moini/ink_addons/-/issues) for it.


# Filter Sets

# Icon Sets

# Palettes

# Symbol Sets

## inkscape-open-symbols

**Author:**  
**Description:**  
**URL:**  
**Inkscape versions:**  
**Operating systems:**  
**Menu location:**  
**Last tested:**  (commit hash, date) by (tester)  
**License:**  
Screenshot

---

# Templates

## inkscape-web-templates

---

# Themes
